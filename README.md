# Tronxy D01(改) Klippe.cfg

## プリンター変更点

Tronxy D01

* BMGタイプのデュアルギアエクストルーダーでダイレクトドライブ化
* ホットエンドはオールメタルで高温対応
* Klipper化
* タッチパネル液晶は取り除いて、OLED+ロータリーエンコーダー

## MCUファームウェア書き込み

1. make menuconfig

   ```
   $ cd ~/klipper
   $ make menuconfig
   ```
   設定
   * Processor model :STM32F103
   * Bootloader offset : 34KiB bootloader (Chitu v6 Bootloader)
   * Clock Reference : 8 MHz crystal
   * Communication interface : Serial (on USART1 PA10/PA9)
   * (250000) Baud rate for serial port

2. make

    ```
    $ make
    ```

3. update.cbdに変換
    ```
    $ ./scripts/update_chitu.py ./out/klipper.bin ./out/update.cbd
    ```

4. ファームウェア書き込み

    SDカードのrootに、update.cbdをコピーし、
    TronxyD01に挿入後、電源ON.


## menu.cfg

* Setup - Calibrationに、
BedScrewAdjustを追加。

    BedScrewAdjustを一通り、メニューで操作できるようなコマンドを入れた。



